# Tools

## FAQ

### What are the RPG Tools ?
A set of tools which allow to you create a 2D RPG or supports your existing game by providing tools you can use on top of it.

### Are there any tutorials ?
I frequently post updates in my Youtube channel : https://www.youtube.com/channel/UCPTY9yJt46Hn_6lmpg8ezhA which also contains a few tutorial videos.
This is a good one to get started : https://www.youtube.com/watch?v=1DvkZxb2bwQ&t=2177s

Besides that you can find more information in this guide.

### Where can I get help
The best place is the PVGames software channel : https://discord.gg/KtnPFeK

### Where can I download old versions ?
You can see the history of all versions here : 

64 bit : https://gitlab.com/Nightmare_82/charactercreator/-/commits/master/RPG_Tools.zip

32 bit : https://gitlab.com/Nightmare_82/charactercreator/-/commits/master/Rpg_Tools_32.zip

You can grab any version you want there or check for updates / release notes.

### Where can I download the latest version of the tools ?
You can always grab the latest versions here:

64 bit : https://gitlab.com/Nightmare_82/charactercreator/-/blob/master/RPG_Tools.zip

32 bit : https://gitlab.com/Nightmare_82/charactercreator/-/blob/master/RPG_Tools_32.zip 

## Tools

There are several tools inside the RpgTools executable. The two main tools (Character Creator and Level Editor) can be used with any engine you want, the other tools are mostly meant to be used if you want to use this engine to create your game.

### Character Creator

With the Character Creator you can choose between all the units the tool has found in the folder(s) you added. You can view animations, configure custom equipment, configure custom colors and other things.
You can save/load the character configurations using json files so you can easily manage multiple characters.
Also you can export characters with various options to sprite sheets or export sprite frames.

### Level Editor
This is the tutorial for the [Level Editor](LevelEditor.md)

## Scripting
This is the tutorial for the [Scripting Language](Scripting.md)