- [Lua scripting](#lua-scripting)
  - [Overview](#overview)
  - [Level scripts](#level-scripts)
  - [Unit scripts](#unit-scripts)
  - [Object scripts](#object-scripts)
  - [Global functions](#global-functions)
    - [getCurrentDeltaTime](#getcurrentdeltatime)
    - [setVariable](#setvariable)
    - [getVariable](#getvariable)
    - [getStringVariable](#getstringvariable)
    - [addUnit](#addunit)
    - [addCharacter](#addcharacter)
    - [playMusic](#playmusic)
    - [playSound](#playsound)
    - [logMessage](#logmessage)
    - [loadLevel](#loadlevel)
    - [addLight](#addlight)
    - [setLightPosition](#setlightposition)
    - [setLightAlpha](#setlightalpha)
    - [setLightZ](#setlightz)
  - [Global variables](#global-variables)
    - [map](#map)
  - [Types](#types)
    - [Unit](#unit)
      - [Properties](#properties)
        - [id](#id)
        - [direction](#direction)
        - [isAlive](#isalive)
        - [isMoving](#ismoving)
        - [position](#position)
        - [speed](#speed)
        - [team](#team)
        - [Stats](#stats)
      - [Functions](#functions)
        - [startAnimation](#startanimation)
          - [animation](#animation)
          - [playMode](#playmode)
        - [moveTo](#moveto)
        - [moveToArea](#movetoarea)
        - [addExperience](#addexperience)
    - [Object](#object)
        - [id](#id-1)
        - [opacity](#opacity)
        - [visible](#visible)
        - [x](#x)
        - [y](#y)
        - [scale](#scale)
        - [rotation](#rotation)
        - [displayName](#displayname)
        - [interactionCursor](#interactioncursor)
        - [identifier](#identifier)
    - [TileMap](#tilemap)
        - [pixelSizeX](#pixelsizex)
        - [pixelSizeY](#pixelsizey)
        - [tilesX](#tilesx)
        - [tilesY](#tilesy)
        - [unitLayer](#unitlayer)
    - [ObjectLayer](#objectlayer)
        - [objects](#objects)
        - [addObject](#addobject)
# Lua scripting

## Overview
Rpg Tools uses Lua for scripting. You can check [this guide](https://www.tutorialspoint.com/lua/lua_overview.htm) to get started.
[This cheat sheet](http://lua-users.org/files/wiki_insecure/users/thomasl/luarefv51.pdf) is also very helpful.

Every script can have two methods:
- init() is called once, here you can initialize your script if needed
- update() is called every frame.

Example:

```lua
function init()
    x = 100
end

function update()
    x = x + 1
    print('x = ' ..x)
end
```

## Level scripts
You can configure which scripts should run for your level in the events window. There you can define conditions(by default none) and then choose "Run script" as the action and choose one of the scripts(Either "Browse" for your custom scripts or "Browse presets" for builtin scripts)

![alt text](images/LevelEditor_AddLevelScript.png "Add unit script")

In a level script you an access all global variables(see below)

## Unit scripts
When you run a script for a specific unit you can access this unit using the variable **unit**

Example:

```lua
unit:startAnimation("Rest", animationMode.Loop, 1000)
unit.direction = 1
```

## Object scripts
When you run a script for a specific object you can access this object using the variable **object**

You can configure object scripts here:
![alt text](images/LevelEditor_AddObjectScript.png "Add unit script")  

Example:

```lua
object.opacity = 0.5
```
## Global functions

### getCurrentDeltaTime
Returns the current delta time in seconds. This can be useful for things like movement that should depend on the time that has passed since the last frame.

Example:

```lua
-- move object with speed 50 pixels per second
speed = 50
deltaTime = getCurrentDeltaTime()
object.x = object.x + speed * deltaTime
```

### setVariable
Set a global variable. Can be used with strings and numbers

Example:

```lua
-- set the global variable 'Reward' to 10
setVariable('Reward', 10)

-- set the global variable 'CharacterName' to 'Frodo'
setVariable('CharacterName', 'Frodo')

```

### getVariable
Get a global number variable

Example:

```lua
-- store current value of 'Reward' variable
value = getVariable("Reward")
-- set the Reward variable to the current value + 1
setVariable("Reward", value + 1)
```

### getStringVariable
Get a global string variable

Example:

```lua
-- store current value of 'CharacterName' variable
name = getStringVariable("CharacterName")
-- print it to the console window
print('Character name is' ..name)
```

### addUnit
Adds a new unit to the current level

Example:

```lua
-- Create a buck at position 100/200
unit = addUnit('Buck', 100, 200)
```

### addCharacter
Adds a new character to the current level

Example:

```lua
-- Create an Orc(needs to be saved in your projects characters folder) at position 100/200 and set its team to Enemy
unit = addCharacter('SmallOrc_01', 100, 200)
unit.team = UnitTeam.Enemy
```

### playMusic
changes the background music using a path relative to the project audio folder ([ProjectFolder]/data/audio)

```lua
-- play music not looped
playMusic("BRPG_Take_Courage_FULL_Loop.wav", false)

-- play music looped
playMusic("BRPG_Take_Courage_FULL_Loop.wav", true)
```

### playSound
Play a sound using a path relative to the project audio folder ([ProjectFolder]/data/audio)

```lua
playSound("Noise_01.wav")
```

### logMessage
Log a message that can be displayed in the ingame log window

```lua
logMessage("You see a gigantic orc in front of you")
```

### loadLevel
Load a new level and optionally set the area where the hero should be spawned 

```lua
loadLevel("Level_01.json", "StartArea")
```

### addLight
Create a new light and returns a unique light ID.

### setLightPosition
sets the position of a light

### setLightAlpha
sets the alpha value of a light

### setLightZ
sets the z value of a light

Example:

```lua
lightID = addLight()
setLightPosition(lightID, 100, 200)
setLightAlpha(lightID, 0.5)
setLightZ(lightID, 0.1)
```


## Global variables

### map
Using the map variable you can access/change values of the current map.

## Types

### Unit

For each unit you can access a couple of properties / functions.
You can configure unit scripts here:
![alt text](images/LevelEditor_AddUnitScript.png "Add unit script")  

#### Properties

##### id
Get the ID of the unit. This is a unique identifier you can use for some of the functions or for debugging.

Example:

```lua
unitID = unit.id
```

##### direction
Get/set the direction index of the unit(0-7)

Example:

```lua
unit.direction = 1
```

##### isAlive
returs whether the unit is still alive

Example:

```lua
if unit.isAlive then
    print('Unit is alive')
end
```

##### isMoving
returs whether the unit is currently moving

Example:

```lua
if unit.isMoving then
    print('Unit is moving')
end
```

##### position
Get/set the position of the unit

Example:

```lua
if unit.position.x < 100 then
    unit.position.x = unit.position.x + 1
end
```

##### speed
Get/set the movement speed of the unit

Example:

```lua
-- accelerate unit speed unitl 50 pixels per second are reached
if unit.speed < 50 then
    unit.speed = unit.speed + 0.1 * getCurrentDeltaTime()
end
```

##### team
Get/set the team of the unit

| Team | Description |
| -------------                     |:-------------:          |
| unitTeam.Enemy | Enemy of the player |
| unitTeam.Player | Friend of the player |
| unitTeam.Neutral | Doesn't participate in any fights(e.g. animals) |

Example:

```lua
unit.team = unitTeam.Enemy
```

##### Stats
You can access all stats you defined in the Data Editor in Lua by their name. The first character is always using lowercase.


Example:

```lua
unit.mana = 0
if unit.hitpoints > 20 then
    print('Unit has more than 20 hitpoints')
end
```

#### Functions

##### startAnimation
starts a specific animation for the unit

Parameters:

###### animation
Name of the animation, e.g. "idle1"

###### playMode
Mode of the animation:
| Mode                              | Description             |
| -------------                     |:-------------:          |
| animationMode.PlayOnce            | Play the animation once |
| animationMode.Loop                | Play the animation looped |
| animationMode.LoopBackAndForth    | Play the animation looped back and forth. For example a 5 frame animation would play the frames in this order : 0,1,2,3,4 -> 3,2,1 -> 0,1,... |

Example:

```lua
unit:startAnimation("idle1", animationMode.Loop, 1000)
unit:startAnimation("Use Skill", animationMode.PlayOnce, 400)
```
##### moveTo
Lets the unit move to a specific position

Example:

```lua
unit:moveTo(10, 20)
```

##### moveToArea
Lets the unit move to a specific area

Example:

```lua
unit:moveToArea('StartArea')
```

##### addExperience
adds experience for the unit
Example:

```lua
-- increase XP by 100
unit:addExperience(100)
```

### Object

##### id
Get the unique ID of an object(0-1)

Example:

```lua
id = object.id
```

##### opacity
Get/set the opacity of an object(0-1)

Example:

```lua
object.opacity = 0.5
```

##### visible
Get/set the visible state of an object

Example:

```lua
object.visible = false
```

##### x
Get/set the x position of an object

Example:

```lua
object.x = 100
```

##### y
Get/set the y position of an object

Example:

```lua
object.y = 100
```

##### scale
Get/set the scale of an object(1=100%)

Example:

```lua
object.scale = 0.5
```

##### rotation
Get/set the rotation of an object in degrees

Example:

```lua
-- set the rotation to 45 degrees
object.rotation = 45
```

##### displayName
Get/set the display name of an object which can be used by the game

Example:

```lua
object.displayName = "A gigantic orc"
```

##### interactionCursor
Get/set the interaction cursor name of an object. It needs to be one of the cursors you defined in the Data Editor

Example:

```lua
object.interactionCursor = "Loot"
```

##### identifier
Get/set the identifier of an object. This is used by some command like move commands of the tool. For example you can create a group of orcs and set its identifier to "Orcs" and then move the whole group using this identier

Example:

```lua
object.identifier = "Orcs"
```

### TileMap

##### pixelSizeX
Get the map width in pixels

Example:

```lua
-- store the pixel size x of the global map using the variable width
width = map.pixelSizeX
```

##### pixelSizeY
Get the map height in pixels

Example:

```lua
-- store the pixel size x of the global map using the variable height
height = map.pixelSizeY
```

##### tilesX
Get the amount of horizontal tiles

Example:

```lua
tilesX = map.tilesX
```

##### tilesY
Get the amount of vertical tiles

Example:

```lua
tilesY = map.tilesY
```

##### unitLayer
Returns the layer where units are located. You can then use it to search for objects / add objects etc.

Example:

```lua
-- set opacity of all objects in the unit layer to zero
layer =  map.unitLayer

for i = 1, #layer.objects do
    object = layer.objects[i]
    print('Object ' ..object.id)
    object.opacity = 0
end
```

### ObjectLayer

##### objects
returns the list of objects in this layer

Example:

```lua
-- Increase x coordinate of all objects in the unit layer
layer =  map.unitLayer

for i = 1, #layer.objects do
    object = layer.objects[i]
    print('Object ' ..object.id)
    object.x = object.x + 1
end
```

##### addObject
adds a new object to the layer

Example:

```lua
-- Increase x coordinate of all objects in the unit layer
layer =  map.unitLayer
object = layer:addObject()
print('Object ' ..object.id .. ' created')
object.x = 10 
```

